// import React from 'react';
// import { createAppContainer} from 'react-navigation';
// import { createDrawerNavigator } from 'react-navigation-drawer';
// import { createBottomTabNavigator } from 'react-navigation-tabs';
// import { FontAwesome5, FontAwesome } from '@expo/vector-icons';

// import { 
//   Dimensions 
// } from 'react-native';

// import { Feather} from '@expo/vector-icons';
// import {
//   ProfileScreen,
//   MessageScreen,
//   ActivityScreen,
//   ListScreen,
//   ReportScreen
// } from './screen/index';
// import SiderBar from './components/siderBar';

// const DrawerNavigator = createDrawerNavigator({
//   ProfileScreen: {
//     screen: ProfileScreen,
//     navigationOptions: {
//       title: 'Profile',
//       drawerIcon: ({tintColor}) => <Feather name = "user" size={16} color={tintColor}/>
//     }
//   },
//   MessageScreen: {
//     screen: MessageScreen,
//     navigationOptions: {
//       title: 'Message',
//       drawerIcon: ({tintColor}) => <Feather name = "message-square" size={16} color={tintColor}/>
//     }
//   },
//   ActivityScreen: {
//     screen: ActivityScreen,
//     navigationOptions: {
//       title: 'Activity',
//       drawerIcon: ({tintColor}) => <Feather name = "activity" size={16} color={tintColor}/>
//     }
//   },
//   ListScreen: {
//     screen: ListScreen,
//     navigationOptions: {
//       title: 'List',
//       drawerIcon: ({tintColor}) => <Feather name = "list" size={16} color={tintColor}/>
//     }
//   },
//   ReportScreen: {
//     screen: ReportScreen,
//     navigationOptions: {
//       title: 'Report',
//       drawerIcon: ({tintColor}) => <Feather name = "bar-chart" size={16} color={tintColor}/>
//     }
//   }
// },{
//   contentComponent: props => <SiderBar {...props}/>,
//   drawerWidth: Dimensions.get("window").width * 0.85,
//   hideStatusBar: true,

//   contentOptions: {
//     activeBackgroundColor: "rgba(212,118,207,0.2)",
//     activeTintColor: "#531158",
//     itemsContainerStyle: {
//       marginTop: 16,
//       marginHorizontal: 8
//     },
//     itemStyle: {
//       borderRadius: 4
//     }
//   }
// });
// export default createAppContainer(DrawerNavigator);
//----------------------------------------------------------------------------------------
import React from 'react';
import { View, Image,Dimensions, SafeAreaView, ScrollView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { 
  Button, 
  Text, 
  Container,
  Header,
  Left,
  Right,
  Body,
  Icon,
  Title,
  List,
  ListItem
} from 'native-base';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import IMAGE from './src/constants/image';
import { createDrawerNavigator } from 'react-navigation-drawer';

class CustomHeader extends React.Component {
  render () {
    let { title, isHome } = this.props
    return (
          <Header>
            <Left>
              {
                isHome ?
                <Button transparent>
                  <Icon name="menu"/>
                </Button> :
                <Button transparent onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back"/>
                </Button>
              }
             
            </Left>
            <Body>
              <Title>{title}</Title>
            </Body>
            <Right>
              {/* <Button transparent>
                <Icon name="menu"/>
              </Button> */}
            </Right>
          </Header>
    );
  }
}
class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Home " isHome={true}/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>HomeScreen</Text>
        <Button onPress={() => this.props.navigation.navigate('HomeDetail')}>
            <Text>Go to Home detail</Text>
        </Button>
        </View>
      </View>
    );
  }
}
class HomeDetail extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Home Detail" navigation={this.props.navigation}/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Home detail screen</Text>
        </View>
      </View>
    );
  }
}
class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Setting " isHome={true}/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>SettingScreen</Text>
        <Button onPress={() => this.props.navigation.navigate('SettingDetail')}>
            <Text>Go to Settting detail</Text>
        </Button>
        </View>
      </View>
    );
  }
}
class SettingDetail extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Setting detail " navigation={ this.props.navigation }/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Setting detail screen</Text>
        </View>
      </View>
    );
  }
}
class Profile extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Setting detail " navigation={ this.props.navigation }/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Profile screen</Text>
        </View>
      </View>
    );
  }
}
class Login extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader title="Setting detail " navigation={ this.props.navigation }/>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Login screen</Text>
        </View>
      </View>
    );
  }
}
class SideMenu extends React.Component {
  render() {
    return (
      <SafeAreaView style={{flex:1}}>
          <View style={{height: 150, alignItems: 'center', justifyContent: 'cneter'}}>
            {/* <Image 
              source={IMAGE.ICON_USER_DEFAULT}
              style={{height: 120, width: 120, borderRadius: 60}}
            /> */}
          </View>
          <ScrollView>
              <List>
                  <ListItem>
                      <Text>Profile</Text>
                  </ListItem>
                  <ListItem>
                      <Text>Login</Text>
                  </ListItem>
              </List>
          </ScrollView>
      </SafeAreaView>
    );
  }
}
const navigatorOptionHandler = (navigator) => ({
  header: null
});
const HomeStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: navigatorOptionHandler
  },
  HomeDetail: {
    screen: HomeDetail,
    navigationOptions: navigatorOptionHandler
  }
});
const SettingStack = createStackNavigator({
  SettingsScreen: {
    screen: SettingsScreen,
    navigationOptions: navigatorOptionHandler
  },
  SettingDetail: {
    screen: SettingDetail,
    navigationOptions: navigatorOptionHandler
  }
});
const MainTabs = createBottomTabNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({tintColor}) => (
        <Image 
          source={require('./src/image/food-and-restaurant.png')}
          resizeMode="contain" 
          style={{ width: 20, height: 20}}
        />
      )
    }
  },
  Settings: {
    screen: SettingStack,
    navigationOptions: {
      tabBarLabel: 'Setting',
        tabBarIcon: ({tintColor}) => (
        <Image 
          source={require('./src/image/food-and-restaurant.png')}
          resizeMode="contain" 
          style={{ width: 20, height: 20}}
        />
      )
    }
  }
});
const MainStack = createStackNavigator({
  A: {
    screen: MainTabs,
    navigationOptions: navigatorOptionHandler
  },
  B: {
    screen: Profile,
    navigationOptions: navigatorOptionHandler
  },
  C: {
    screen: Profile,
    navigationOptions: navigatorOptionHandler
  } 
}, { 'initialRouteName': 'A' }
);
const appDrawer = createDrawerNavigator(
  {
    drawer: MainStack
  },
  {
    contentComponent: SideMenu,
    drawerWidth: (Dimensions.get('window').width * 0.85)
  }
)

export default createAppContainer(appDrawer);
