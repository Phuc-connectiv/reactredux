import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    ImageBackground,
    Image
} from 'react-native';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import {  Ionicons } from '@expo/vector-icons' ;

export default SiderBar = (props) => {
    return (
        <ScrollView>
            <ImageBackground 
            source={require('../assets/background.jpg')} 
            style={{width: undefined, padding: 14, paddingTop: 48}}>
                <Image source={require('../assets/profile.jpg')} style={styles.profile}/>
                <Text style={styles.text}>Em yeu</Text>
                <View>
                    <Text style={styles.flower}> 1000 Followers</Text>
                    <Ionicons name='md-people' size={16} color='rgba(225,225,225,0.8)'/>
                </View>
            </ImageBackground>
            <View>
                <DrawerNavigatorItems {...props}/>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    profile: {
       flex: 1
    },
    profile: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: '#fff'
    },
    text: {
        color: '#fff',
        fontSize: 20,
        fontWeight:'bold',
        marginVertical: 5
    },
    flower: {
        color: 'rgba(225,225,225, 0.8)',
        fontSize: 13,
        marginRight: 4
    }
});